FROM maven:3.6.3-jdk-11-slim AS MAVEN_BUILD



ARG SPRING_ACTIVE_PROFILE

MAINTAINER popasmuerf
COPY pom.xml /build/
COPY src /build/src/
WORKDIR /build/
RUN mvn clean install -Dspring.profiles.active=$SPRING_ACTIVE_PROFILE && mvn package -B -e -Dspring.profiles.active=$SPRING_ACTIVE_PROFILE
FROM openjdk:11-slim
#FROM openjdk:8-alpine FOR JAVA 8
WORKDIR /app
COPY --from=MAVEN_BUILD /build/target/demofoo-*.jar /app/demofoo.jar
ENTRYPOINT ["java", "-jar", "demofoo.jar"]

