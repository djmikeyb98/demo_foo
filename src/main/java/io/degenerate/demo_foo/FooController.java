package io.degenerate.demo_foo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FooController {
@RequestMapping(value="/foo")
    public String getFoo(){
        return("FOO!!!!");
    }
}
