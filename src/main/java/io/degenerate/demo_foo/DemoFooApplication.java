package io.degenerate.demo_foo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoFooApplication {


	public static void main(String[] args) {
		SpringApplication.run(DemoFooApplication.class, args);
	}

}
